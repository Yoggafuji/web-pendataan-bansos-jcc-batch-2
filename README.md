# web-pendataan-bansos-jcc-batch-2

Nama	:	Yogga Fujianto Sanjaya, 
Kelas	:	VueJS, 
Video Demo	:	https://youtu.be/PGsmeV2uKKk,
Link Website	:	https://curious-parfait-08533c.netlify.app/,



Tentang Layout/Desain
Layout web aplikasi yang saya buat ini sangat sederhana, dengan layout hanya app bar diatas dan formulir saja. Saya terinspirasi dari google form, namun dikarenakan web ini akan digunakan oleh user yang rata-rata usianya diatas 40 tahun, saya membuat desain formulir yang simpel, dengan disertai warna dasar abu-abu dan hitam, juga dengan beberapa aksen warna cerah yang saya harap bisa membuat user bisa melihat jelas kontennya/formulitnya.

Lalu saya juga menggunakan Form input yang ada outlined lalu terdapat label yang berada diatas dan berukuran lebih kecil daripada Kolom Input, tujuannya agar user lebih mudah menemukan mana kolom yang harus diisi. Kemudian untuk memudahkan user dalam pengisian formulir, saya menggunakan placeholder yang berisikan contoh atau keterangan apa yang harus diisikan atau apa yang harus dilakukan pengguna untuk mengisi kolom tersebut.

Untuk memastikan apakah formulir sudah diisi, saya memberikan setiap kolom input dengan fitur required,jadi user akan tahu mana kolom yang harus diisi. lalu terdapat juga fitur validasi, guna mengecek apakah user telah mengisi dengan benar, jika terdapat kesalahan dalam proses pengisian formulir maka user tidak bisa mensubmit formulirnya.

Kolom Input
Saya menggunakan jenis kolom input dengan posisi label berada diatas dan berukuran lebih kecil daripada Kolom Input, yang bertujuan supaya pengguna lebih mudah melihat mana kolom untuk diisi dan mana label.
Dalam mempermudah pengisian sama menggunakan placeholder yang berisikan contoh atau keterangan apa yang harus diisikan atau apa yang harus dilakukan pengguna untuk mengisi kolom tersebut.

Jika semua formulir sudah diisi dengan benar lalu di submit oleh user maka web akan menampilkan dialog untuk mengetahui status pengiriman formulir apakah berhasil atau gagal. Jika submit gagal, maka akan muncul dialog dengan informasi "gagal menyimpan" serta terdapat tombol "simpan kembali" untuk memudahkan user mensubmit ulang formulirnya tanpa harus mengisi ulan formlirnya. Lalu jika submit berhasil maka akan muncul dialog "berhasil Disimpan" serta terdapat tombol "tambah daftar baru" yang akan memudahkan user untuk menambahkan data baru.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
